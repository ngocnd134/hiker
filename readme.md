# Hệ thống đi bộ

## Tổng quan hệ thống
- Hệ thống cho phép thu thập và thống kê số bước chân của người dùng
- Các chức năng chính: Bảng xếp hạng (realtime) và thống kê số bước chân theo từng ngày, tuần, tháng hiện tại
- Hệ thống bao gồm các thành phần chính sau:
  - Load balancer: Chia tải các request từ client đến server
  - Cụm server: Thực thi các logic tính toán
  - PostgreSQL: Lưu trữ các thông tin đi bộ của người dùng theo ngày, tuần, tháng
  - Redis: Lưu trữ thông tin số bước đi bộ của người dùng trong ngày, để tính toán ra bảng xếp hạng

![architecture](architecture.png)
## Thiết kế database
- PostgeSQL có 3 bảng:
  - daily_step: để lưu số bước chân của người dùng theo ngày
  - weekly_step: để lưu số bước chân của người dùng theo tuần
  - monthly_step: để lưu số bước chân của người dùng theo

![Database](database.png "database")
- Redis: Lưu trữ bảng xếp hạng theo ngày, sử dụng sorted set, với score là số bước chân trong ngày,
value là userId
## Công nghệ sử dụng
- Java
- Vertx
- PostgreSQL
- Redis
