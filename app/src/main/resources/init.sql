-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler version: 1.1.0-alpha
-- PostgreSQL version: 14.0
-- Project Site: pgmodeler.io
-- Model Author: ---

-- Database creation must be performed outside a multi lined SQL file. 
-- These commands were put in this file only as a convenience.
-- 
-- object: hiker | type: DATABASE --
-- DROP DATABASE IF EXISTS hiker;
-- CREATE DATABASE hiker;
-- ddl-end --


-- object: "uuid-ossp" | type: EXTENSION --
-- DROP EXTENSION IF EXISTS "uuid-ossp" CASCADE;
CREATE EXTENSION "uuid-ossp"
WITH SCHEMA public
VERSION '1.1';
-- ddl-end --

-- object: public.daily_step | type: TABLE --
-- DROP TABLE IF EXISTS public.daily_step CASCADE;
CREATE TABLE public.daily_step (
	id uuid NOT NULL DEFAULT uuid_generate_v1(),
	user_id uuid NOT NULL,
	date date NOT NULL,
	step_number int8 NOT NULL,
	CONSTRAINT daily_step_pk PRIMARY KEY (id),
	CONSTRAINT date_user_id_unq UNIQUE (date,user_id)
);
-- ddl-end --

-- object: public.weekly_step | type: TABLE --
-- DROP TABLE IF EXISTS public.weekly_step CASCADE;
CREATE TABLE public.weekly_step (
	id uuid NOT NULL DEFAULT uuid_generate_v1(),
	user_id uuid NOT NULL,
	week smallint NOT NULL,
	year smallint NOT NULL,
	step_number int8 NOT NULL,
	CONSTRAINT weekly_step_pk PRIMARY KEY (id),
	CONSTRAINT week_year_userid_unq UNIQUE (year,week,user_id)
);
-- ddl-end --

-- object: public.monthly_step | type: TABLE --
-- DROP TABLE IF EXISTS public.monthly_step CASCADE;
CREATE TABLE public.monthly_step (
	id uuid NOT NULL DEFAULT uuid_generate_v1(),
	user_id uuid NOT NULL,
	month smallint NOT NULL,
	year smallint NOT NULL,
	step_number int8 NOT NULL,
	CONSTRAINT monthly_step_pk PRIMARY KEY (id),
	CONSTRAINT year_month_user_id_unq UNIQUE (year,month,user_id)
);
-- ddl-end --


