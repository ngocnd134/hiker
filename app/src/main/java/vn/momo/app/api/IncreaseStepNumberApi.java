package vn.momo.app.api;

import com.google.inject.Inject;
import io.vertx.ext.web.RoutingContext;
import java.util.UUID;
import java.util.concurrent.CompletionStage;
import lombok.AllArgsConstructor;
import vn.momo.FootStepHandler;

@AllArgsConstructor(onConstructor_ = @Inject)
@ApiRegister(method = "post", path = "/v1/footStep/increase")
public class IncreaseStepNumberApi extends AbstractApi {

  private final FootStepHandler footStepHandler;

  @Override
  protected CompletionStage<?> handleRequest(RoutingContext context) {
    var body = context.body().asJsonObject().mapTo(Body.class);
    return footStepHandler.increaseStepNumber(body.userId, body.stepNumber);
  }

  record Body(UUID userId, int stepNumber) {

  }
}
