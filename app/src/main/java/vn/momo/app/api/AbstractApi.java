package vn.momo.app.api;

import static java.util.Objects.nonNull;

import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import java.util.concurrent.CompletionStage;

public abstract class AbstractApi implements Handler<RoutingContext> {

  public void handle(RoutingContext context) {
    handleRequest(context)
        .whenComplete((res, e) -> {
          if (nonNull(e)) {
            handleException(e, context);
            return;
          }
          returnResponse(context, res);
        });
  }

  protected abstract CompletionStage<?> handleRequest(RoutingContext context);

  private void returnResponse(RoutingContext context, Object res) {
    var response = context.response();
    response.putHeader("content-type", "application/json; charset=utf-8");
    response.end(new JsonObject().put("data", res).toBuffer());
  }

  private void handleException(Throwable throwable, RoutingContext context) {
    context.response().putHeader("content-type", "application/json; charset=utf-8");
    context.response().send(new JsonObject().put("error", throwable.getMessage()).toBuffer());
  }

}
