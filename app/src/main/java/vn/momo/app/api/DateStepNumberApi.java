package vn.momo.app.api;

import com.google.inject.Inject;
import io.vertx.ext.web.RoutingContext;
import java.util.UUID;
import java.util.concurrent.CompletionStage;
import lombok.AllArgsConstructor;
import vn.momo.FootStepHandler;

@AllArgsConstructor(onConstructor_ = @Inject)
@ApiRegister(method = "get", path = "/v1/footStep/currentDate/:userId")
public class DateStepNumberApi extends AbstractApi {

  private final FootStepHandler footStepHandler;

  @Override
  protected CompletionStage<?> handleRequest(RoutingContext context) {
    var userId = UUID.fromString(context.pathParam("userId"));
    return footStepHandler.getCurrentDay(userId);
  }
}
