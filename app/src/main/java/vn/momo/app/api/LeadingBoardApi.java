package vn.momo.app.api;

import com.google.inject.Inject;
import io.vertx.ext.web.RoutingContext;
import java.util.UUID;
import java.util.concurrent.CompletionStage;
import lombok.AllArgsConstructor;
import vn.momo.FootStepHandler;

@AllArgsConstructor(onConstructor_ = @Inject)
@ApiRegister(method = "get", path = "/v1/leadingBoard/:userId")
public class LeadingBoardApi extends AbstractApi {

  private final FootStepHandler footStepHandler;

  @Override
  protected CompletionStage<?> handleRequest(RoutingContext context) {
    var userId = UUID.fromString(context.pathParam("userId"));
    var limit = Integer.parseInt(context.queryParams().get("limit"));
    return footStepHandler.getLeadingBoard(userId, limit);
  }

}
