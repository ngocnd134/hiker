package vn.momo.app.module;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.vertx.core.Vertx;
import io.vertx.pgclient.PgConnectOptions;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.PoolOptions;
import lombok.AllArgsConstructor;
import vn.momo.FootStepRepo;
import vn.momo.app.AppConfig;
import vn.momo.repo.PgFootStepRepo;

@AllArgsConstructor
public class AppModule extends AbstractModule {

  private final Vertx vertx;
  private final AppConfig config;

  @Override
  protected void configure() {
    bind(Vertx.class).toInstance(vertx);
    bind(AppConfig.class).toInstance(config);
  }

  @Provides
  @Singleton
  PgPool pgPool(Vertx vertx, AppConfig config) {
    var databaseConfig = config.getDatabase();
    var connectOptions = new PgConnectOptions()
        .setPort(databaseConfig.getPort())
        .setHost(databaseConfig.getHost())
        .setDatabase(databaseConfig.getDatabase())
        .setUser(databaseConfig.getUser())
        .setPassword(databaseConfig.getPassword());

    var poolOptions = new PoolOptions()
        .setMaxSize(databaseConfig.getMaxPoolSize());
    return PgPool.pool(vertx, connectOptions, poolOptions);
  }

  @Provides
  @Singleton
  StatefulRedisConnection<String, String> redisConnection(AppConfig config) {
    RedisClient redisClient = RedisClient.create(config.getRedisUri());
    return redisClient.connect();
  }

  @Provides
  @Singleton
  FootStepRepo footStepRepo(PgPool pgPool,
      StatefulRedisConnection<String, String> redisConnection) {
    return new PgFootStepRepo(pgPool, redisConnection);
  }

}
