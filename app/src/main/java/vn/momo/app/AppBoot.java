package vn.momo.app;

import com.google.inject.Guice;
import io.vertx.core.Vertx;
import java.util.concurrent.CompletionStage;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.utility.DockerImageName;
import vn.momo.app.AppConfig.PgConfig;
import vn.momo.app.module.AppModule;
import vn.momo.app.verticle.HttpServerVerticle;

public class AppBoot {

  public static void main(String[] args) {
    start();
  }

  public static CompletionStage<Void> start() {
    var config = mockInfra();
    var vertx = Vertx.vertx();
    var injector = Guice.createInjector(new AppModule(vertx, config));
    var httpServer = new HttpServerVerticle(injector);
    injector.getInstance(Vertx.class);
    return vertx.deployVerticle(httpServer)
        .toCompletionStage()
        .thenAccept(any -> {
        });
  }

  public static AppConfig mockInfra() {
    var redisContainer = new GenericContainer(
        DockerImageName.parse("redis:5.0.3-alpine"))
        .withExposedPorts(6379);
    var pgContainer = new PostgreSQLContainer<>("postgres:14")
        .withDatabaseName("hiker")
        .withUsername("hiker");

    redisContainer.start();
    pgContainer
        .withInitScript("init.sql")
        .start();
    return AppConfig.builder()
        .redisUri("redis://localhost:%d/0".formatted(redisContainer.getMappedPort(6379)))
        .database(PgConfig.builder()
            .database(pgContainer.getDatabaseName())
            .user(pgContainer.getUsername())
            .password(pgContainer.getPassword())
            .host(pgContainer.getHost())
            .port(pgContainer.getMappedPort(5432))
            .maxPoolSize(10)
            .build())
        .build();
  }

}
