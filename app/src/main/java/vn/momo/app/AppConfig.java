package vn.momo.app;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AppConfig {

  private PgConfig database;
  private String redisUri;

  @Getter
  @Builder
  @NoArgsConstructor
  @AllArgsConstructor
  public static class PgConfig {

    private String host;
    private int port;
    private String user;
    private String password;
    private String database;
    private int maxPoolSize;
  }

}
