package vn.momo.app.verticle;

import com.google.inject.Injector;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import lombok.AllArgsConstructor;
import org.reflections.Reflections;
import vn.momo.app.api.ApiRegister;

@AllArgsConstructor
public class HttpServerVerticle extends AbstractVerticle {

  private final Injector injector;

  public void start(Promise<Void> startPromise) {
    var router = Router.router(vertx);
    var bodyHandler = BodyHandler.create();
    router.route().handler(bodyHandler);
    registerRouter(router);
    var server = vertx.createHttpServer().requestHandler(router);
    server.listen(8080, res -> {
      if (res.succeeded()) {
        startPromise.complete();
      } else {
        startPromise.fail(res.cause());
      }
    });
  }

  @SuppressWarnings("unchecked")
  public void registerRouter(Router router) {
    var reflection = new Reflections("vn.momo");
    for (var clazz : reflection.getTypesAnnotatedWith(ApiRegister.class)) {
      var annotation = clazz.getAnnotation(ApiRegister.class);
      var handler = (Handler<RoutingContext>) injector.getInstance(clazz);
      router.route(HttpMethod.valueOf(annotation.method().toUpperCase()), annotation.path())
          .handler(handler);
    }
  }

}
