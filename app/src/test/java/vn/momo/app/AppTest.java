package vn.momo.app;

import static io.restassured.RestAssured.given;

import io.vertx.core.json.JsonObject;
import java.util.UUID;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class AppTest {

  private static final String BASE_URI = "http://localhost:8080";

  @BeforeAll
  @SneakyThrows
  static void setup() {
    AppBoot.start();
  }

  @Test
  @DisplayName("Save step number successfully")
  void t1() {
    saveStepNumber(UUID.randomUUID(), 10);
  }

  private void saveStepNumber(UUID userId, int stepNumber) {
    var body = new JsonObject()
        .put("userId", userId)
        .put("stepNumber", stepNumber);
    given()
        .baseUri(BASE_URI)
        .body(body.encode())
        .post("/v1/footStep")
        .then()
        .log()
        .all()
        .statusCode(200);
  }

  @Test
  @DisplayName("Get step number of current date successfully")
  void t2() {
    var userId = UUID.randomUUID();
    saveStepNumber(userId, 11);
    var actual = given()
        .log().all()
        .baseUri(BASE_URI)
        .get("/v1/footStep/currentDate/" + userId)
        .then()
        .log()
        .all()
        .statusCode(200)
        .extract()
        .path("data");
    Assertions.assertEquals(11, Integer.parseInt(actual.toString()));
  }

  @Test
  @DisplayName("Get step number of current week successfully")
  void t3() {
    var userId = UUID.randomUUID();
    saveStepNumber(userId, 13);
    var actual = given()
        .log().all()
        .baseUri(BASE_URI)
        .get("/v1/footStep/currentWeek/" + userId)
        .then()
        .log()
        .all()
        .statusCode(200)
        .extract()
        .path("data");

    Assertions.assertEquals(13, Integer.parseInt(actual.toString()));
  }

  @Test
  @DisplayName("Get step number of current month successfully")
  void t4() {
    var userId = UUID.randomUUID();
    saveStepNumber(userId, 13);
    var actual = given()
        .log().all()
        .baseUri(BASE_URI)
        .get("/v1/footStep/currentMonth/" + userId)
        .then()
        .log()
        .all()
        .statusCode(200)
        .extract()
        .path("data");

    Assertions.assertEquals(13, Integer.parseInt(actual.toString()));
  }

  @Test
  @DisplayName("Get step number of current month successfully")
  void t5() {
    for (int i = 1000; i > 900; i--) {
      saveStepNumber(UUID.randomUUID(), i);
    }
    var userId = UUID.randomUUID();
    given()
        .log().all()
        .baseUri(BASE_URI)
        .queryParam("limit", 100)
        .get("/v1/leadingBoard/" + userId)
        .then()
        .log()
        .all()
        .statusCode(200);
  }

}
