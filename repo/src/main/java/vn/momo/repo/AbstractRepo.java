package vn.momo.repo;

import io.vertx.pgclient.PgConnection;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.RowSet;
import io.vertx.sqlclient.impl.ArrayTuple;
import java.util.concurrent.CompletionStage;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class AbstractRepo {

  protected final PgPool pgPool;

  protected CompletionStage<RowSet<Row>> executeThenGet(String sql, Object... args) {
    return pgPool.getConnection().toCompletionStage()
        .thenCompose(conn -> {
              var pgConn = (PgConnection) conn;
              var tuple = new ArrayTuple(args.length);
              for (var arg : args) {
                tuple.addValue(arg);
              }
              return pgConn.preparedQuery(sql)
                  .execute(tuple)
                  .toCompletionStage()
                  .thenCompose(rows -> pgConn.close().toCompletionStage()
                      .thenApply(any -> rows));
            }
        );
  }

}
