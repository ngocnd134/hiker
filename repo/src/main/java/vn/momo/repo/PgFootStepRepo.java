package vn.momo.repo;

import io.lettuce.core.ScoredValue;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.async.RedisAsyncCommands;
import io.vertx.pgclient.PgPool;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.temporal.WeekFields;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import vn.momo.FootStepRepo;
import vn.momo.LeadingBoard;
import vn.momo.UserStep;

public class PgFootStepRepo extends AbstractRepo implements FootStepRepo {

  private static final String LEADING_BOARD_KEY_PREFIX = "leadingBoard";

  private final RedisAsyncCommands<String, String> cmd;

  public PgFootStepRepo(PgPool pgPool, StatefulRedisConnection<String, String> redisConnection) {
    super(pgPool);
    this.cmd = redisConnection.async();
  }

  @Override
  public CompletionStage<Void> increaseStepNumber(UUID userId, LocalDate date, int stepNumber) {
    var weekFields = WeekFields.of(Locale.getDefault());
    int week = date.get(weekFields.weekOfWeekBasedYear());
    var month = date.getMonthValue();
    var year = date.getYear();

    return executeThenGet("""
        WITH day AS (
            INSERT INTO daily_step(user_id, date, step_number)
            VALUES($1, $2, $3)
            ON CONFLICT (date, user_id)
            DO UPDATE SET step_number = daily_step.step_number + $3
        ),
        week AS (
            INSERT INTO weekly_step(user_id, week, year, step_number)
            VALUES($1, $4, $6, $3)
            ON CONFLICT (year, week, user_id)
            DO UPDATE SET step_number = weekly_step.step_number + $3
        )
        INSERT INTO monthly_step(user_id, month, year, step_number)
        VALUES($1, $5, $6, $3)
        ON CONFLICT (year, month, user_id)
        DO UPDATE SET step_number = monthly_step.step_number + $3
        """, userId, date, stepNumber, week, month, year)
        .thenCompose(any -> {
          var key = getLeadingBoardCacheKey(date);
          return cmd.zaddincr(key, stepNumber, userId.toString())
              .thenCompose(__ -> cmd.expireat(key, getEndOfDay(date)));
        }).thenAccept(__ -> {
        });
  }

  @Override
  public CompletionStage<Void> save(UUID userId, LocalDate date, int stepNumber) {
    var weekFields = WeekFields.of(Locale.getDefault());
    int week = date.get(weekFields.weekOfWeekBasedYear());
    var month = date.getMonthValue();
    var year = date.getYear();

    return executeThenGet("""
        WITH currentStep AS (
            SELECT * FROM daily_step WHERE user_id = $1 AND date = $2
        ),
        day AS (
            INSERT INTO daily_step(user_id, date, step_number)
            VALUES($1, $2, $3)
            ON CONFLICT (date, user_id)
            DO UPDATE SET step_number = daily_step.step_number - (SELECT step_number FROM currentStep) + $3
        ),
        week AS (
            INSERT INTO weekly_step(user_id, week, year, step_number)
            VALUES($1, $4, $6, $3)
            ON CONFLICT (year, week, user_id)
            DO UPDATE SET step_number = weekly_step.step_number - (SELECT step_number FROM currentStep) + $3
        )
        INSERT INTO monthly_step(user_id, month, year, step_number)
        VALUES($1, $5, $6, $3)
        ON CONFLICT (year, month, user_id)
        DO UPDATE SET step_number = monthly_step.step_number - (SELECT step_number FROM currentStep) + $3
        """, userId, date, stepNumber, week, month, year)
        .thenCompose(any -> {
          var key = getLeadingBoardCacheKey(date);
          return cmd.zadd(key,
                  ScoredValue.from(stepNumber, Optional.of(userId.toString())))
              .thenCompose(__ -> cmd.expireat(key, getEndOfDay(date)));
        })
        .thenAccept(any -> {
        });
  }

  @Override
  public CompletionStage<Long> getByDate(UUID userId, LocalDate date) {
    return executeThenGet("SELECT * FROM daily_step WHERE user_id = $1 AND date = $2", userId, date)
        .thenApply(rows -> {
          var it = rows.iterator();
          if (it.hasNext()) {
            return it.next().getLong("step_number");
          }
          return null;
        });
  }

  @Override
  public CompletionStage<Long> getByWeek(UUID userId, int week) {
    return executeThenGet("SELECT * FROM weekly_step WHERE user_id = $1 AND week = $2", userId,
        week)
        .thenApply(rows -> {
          var it = rows.iterator();
          if (it.hasNext()) {
            return it.next().getLong("step_number");
          }
          return null;
        });
  }

  @Override
  public CompletionStage<Long> getByMonth(UUID userId, int month) {
    return executeThenGet("SELECT * FROM monthly_step WHERE user_id = $1 AND month = $2", userId,
        month)
        .thenApply(rows -> {
          var it = rows.iterator();
          if (it.hasNext()) {
            return it.next().getLong("step_number");
          }
          return null;
        });
  }

  @Override
  public CompletionStage<LeadingBoard> getLeadingBoard(UUID userId, int limit) {
    var key = getLeadingBoardCacheKey(LocalDate.now());
    var rangeFuture = cmd.zrevrangeWithScores(key, 0, limit)
        .toCompletableFuture();
    var rankFuture = cmd.zrevrank(key, userId.toString())
        .toCompletableFuture();
    return CompletableFuture.allOf(rangeFuture, rangeFuture)
        .thenApply(any -> {
          var range = rangeFuture.getNow(List.of());
          var rank = rankFuture.getNow(null);
          var userSteps = range.stream()
              .map(scoredValue -> new UserStep(UUID.fromString(scoredValue.getValue()),
                  Double.valueOf(scoredValue.getScore()).longValue())).toList();
          return new LeadingBoard(rank, userSteps);
        });
  }

  private String getLeadingBoardCacheKey(LocalDate date) {
    return LEADING_BOARD_KEY_PREFIX + date.toString();
  }

  private Instant getEndOfDay(LocalDate date) {
    return date.atTime(LocalTime.MAX)
        .atZone(ZoneOffset.systemDefault())
        .toInstant();
  }
}
