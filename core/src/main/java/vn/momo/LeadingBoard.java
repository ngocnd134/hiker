package vn.momo;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class LeadingBoard {

  private Long currentRank;
  private List<UserStep> userSteps;
}
