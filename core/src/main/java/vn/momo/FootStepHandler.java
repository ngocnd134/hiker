package vn.momo;

import com.google.inject.Inject;
import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.CompletionStage;
import lombok.AllArgsConstructor;

@AllArgsConstructor(onConstructor_ = @Inject)
public class FootStepHandler {

  private FootStepRepo footStepRepo;

  public CompletionStage<Void> increaseStepNumber(UUID userId, int number) {
    return footStepRepo.increaseStepNumber(userId, LocalDate.now(), number);
  }

  public CompletionStage<Void> save(UUID userId, int number) {
    return footStepRepo.save(userId, LocalDate.now(), number);
  }

  public CompletionStage<Long> getCurrentDay(UUID userId) {
    var date = LocalDate.now();
    return footStepRepo.getByDate(userId, date);
  }

  public CompletionStage<Long> getCurrentWeek(UUID userId) {
    var date = LocalDate.now();
    var weekFields = WeekFields.of(Locale.getDefault());
    int week = date.get(weekFields.weekOfWeekBasedYear());
    return footStepRepo.getByWeek(userId, week);
  }

  public CompletionStage<Long> getCurrentMonth(UUID userId) {
    return footStepRepo.getByMonth(userId, LocalDate.now().getMonthValue());
  }

  public CompletionStage<LeadingBoard> getLeadingBoard(UUID userId, int limit) {
    return footStepRepo.getLeadingBoard(userId, limit);
  }
}
