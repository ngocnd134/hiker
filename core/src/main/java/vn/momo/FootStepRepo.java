package vn.momo;

import java.time.LocalDate;
import java.util.UUID;
import java.util.concurrent.CompletionStage;

public interface FootStepRepo {

  CompletionStage<Void> increaseStepNumber(UUID userId, LocalDate localDate, int stepNumber);

  CompletionStage<Void> save(UUID userId, LocalDate localDate, int stepNumber);

  CompletionStage<Long> getByDate(UUID userId, LocalDate date);

  CompletionStage<Long> getByWeek(UUID userId, int week);

  CompletionStage<Long> getByMonth(UUID userId, int month);

  CompletionStage<LeadingBoard> getLeadingBoard(UUID userId, int limit);

}
